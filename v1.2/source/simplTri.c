//***************************************************************************************************************************
// nanoSCULPT: A tool to generate complex and realistic configurations for atomistic simulations
// Copyright (C) 2011-2015
// @authors: A. Prakash, M. Hummel, S. Schmauder and E. Bitzek, 
//
//
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 * 
 */
//***************************************************************************************************************************

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include "mk_config_globals.h"

void simpleTriangulation(void);

/*---------------------------------------------------------------*/

void simpleTriangulation(void)
{
  int whileCount; //debugging only

  int i,j,k,l,m,n;
  int triangleCount = 0; //counts number of triangles
  int buffer[POLNRMX] = {0},buffer2[POLNRMX] = {0};
  bool notList[POLNRMX];//false = not in polygon, true = is in polygon
  int trueCount;
  for(i=0;i< numPolygon; i++)
    {
      k=0;
      n=0;
      trueCount=0;

      /*initialising true List */
      for(j=0;j<POLNRMX;j++)
	{
	  notList[j]=false;
	  if(polygon[i][j] != 0 )
	    {
	      notList[j]=true;
	      trueCount++;
	    }
	 }
      /*initialising buffer*/
      m=0;
      for(n=0; n< POLNRMX;n++)
	{
	  if(notList[n]==true)
	    {
	      buffer[m]=polygon[i][n];
	      m++;
	    }
	}
      whileCount = 0;
      while(trueCount>2)
	{
	  whileCount++;
	  m=0;
	  for(n=0; n< POLNRMX; n++)
	    {
	      if(buffer[n]!=0)
		{
		  notList[n]=true;
		  m++;
		}
	      else notList[n]=false;
	    }
	  trueCount=m;
	  k=m;
	 /*forms a triangle, and eliminates the middle vertex */
	  for(l=1;l<k-1; l+=2)
	    {
	      triangles[triangleCount].x = buffer[l-1];
	      triangles[triangleCount].y = buffer[l];
	      triangles[triangleCount].z = buffer[l+1];
	      notList[l]=false;
	      trueCount = trueCount - 1;
	      triangleCount++;
	    }
       /*closes the circle */
	  if(k%2==0 &&  trueCount>2)
	    {
	      triangles[triangleCount].x = buffer[k-2];
	      triangles[triangleCount].y = buffer[k-1];
	      triangles[triangleCount].z = buffer[0];
	      triangleCount++;
	      notList[k-1]=false;
	      trueCount+=-1;
	    }
	  m=0;
	    
	  for(n=0; n< POLNRMX; n++)
	    {
	      buffer2[n] = 0;
	      if(notList[n]==true)
		{
		  buffer2[m]=buffer[n];
		  m++;
		}
	    }  

	  for(n=0; n< POLNRMX; n++)
	    {
	      buffer[n]=0;
	      if(buffer2[n]!=0)
		{
		  buffer[n]=buffer2[n];
		}
	    }
	}
    }
  trianglesNumber=triangleCount;
} 
