//***************************************************************************************************************************
// nanoSCULPT: A tool to generate complex and realistic configurations for atomistic simulations
// Copyright (C) 2011-2015
// @authors: A. Prakash, M. Hummel, S. Schmauder and E. Bitzek, 
//
//
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 * 
 */
/* mk_config_globals.h
   Global variables for mk_config.c */
//***************************************************************************************************************************


#include <stdio.h> 

typedef double real;
typedef struct {double x; double y; double z; char type; } vektor;
typedef struct {int x; int y; int z; } intvektor; /*new*/ 
typedef char str255[255];

#define INIT(data) =data
#define nullvektor  { 0.0, 0.0, 0.0 }
#define POLNRMX 14 //maximum number of vertices in a polygon

str255 struktur;

str255 pipchar;

str255 outfile;
str255 eingabe;   /*new*/
vektor verticesFile[4000000];  /*new*/
intvektor faces[4000000];     /*new*/
vektor bufferAtom;    //new
FILE *out, *out2, *out3;
int numFaces, numVertices, numPolygon;  //new
intvektor lower;
double e;

int start,type;
int nbasisvects;
int basistype[17];

double m,a,c;
double m1;

vektor box_x,box_y,box_z; // arun: changed from double to vektor!
int header; // arun: flag for header write to outfile
 
vektor shiftvec;
vektor center_shift; // Arun: flag to shift the center for filling atoms
int custom_box;      // Arun: flag for using custom box sizes
int idebug; //Arun: Debug flag - provides more output!
vektor box_minimal;
vektor box_maximal;
vektor n1,n2;
vektor point1,point2;
double d1;
double d2;
double cutdir1,cutdir2;

vektor nx;
vektor ny;
vektor nz;
vektor m1_x,m1_y,m1_z;

double winkel1;
double winkel2;
vektor axe1;
vektor axe2; 
double drehmatrix1[3][3];
double drehmatrix2[3][3];
vektor bminimal;   //new
vektor bmaximal;   //new
vektor boxCenter;  //new
//vektor N;//normalvector global
int polygon[4000000][POLNRMX]; //for 100 faces, each max POLNRMX vertices
intvektor triangles[4000000]; 
int trianglesNumber;   

#define DIM 3                  /* Dimension of points */
typedef int    tPointi[DIM];   /* Type integer point */
typedef double tPointd[DIM];   /* Type double point */
#define PMAX 4000000             /* Max # of pts */
tPointd Vertices[PMAX];        /* All the points */
tPointi Faces[PMAX];
tPointd Box[PMAX][2]; 
tPointd coordSystem[3];
int maxpolyedervertices;

