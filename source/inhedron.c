//***************************************************************************************************************************
// nanoSCULPT: A tool to generate complex and realistic configurations for atomistic simulations
// Copyright (C) 2011-2015
// @authors: A. Prakash, M. Hummel, S. Schmauder and E. Bitzek, 2011
// NOTE: The following code (all code lines in this file) is originally from Joseph O'Rourke, with contributions by Min Xu...
// NOTE: ... and can be found in the book "Computational Geometry in C". For more details see the comment lines below
// NOTE: The original code has been slightly modified to suit the needs of nanoSCULPT. For the modifications undertaken...
// NOTE: ...see comment lines below.
//
//§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§
/* 
This code is described in "Computational Geometry in C" (Second Edition),
Chapter 7.  It is not written to be comprehensible without the
explanation in that book.

Compile:    gcc -o inhedron inhedron.c -lm (or simply: make)
Run (e.g.): inhedron < i.8

Written by Joseph O'Rourke, with contributions by Min Xu.
Last modified: April 1998
Questions to orourke@cs.smith.edu.
--------------------------------------------------------------------
This code is Copyright 1998 by Joseph O'Rourke.  It may be freely
redistributed in its entirety provided that this copyright notice is
not removed.
--------------------------------------------------------------------
//§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§
*/
//
/*
Modified by Martin Hummel November 2011.
Modification is to make float vertices available and to get connected to an other tool named Nanosculpt
Modified by Arun to account for the right periodic vectors
 */
//***************************************************************************************************************************
#include <stdlib.h>  
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "mk_config_globals.h"
#define EXIT_FAILURE 1
#define X 0
#define Y 1
#define Z 2
#define MAX_INT   2147483647 
typedef enum { FALSE, TRUE } bool;

int check = 0;


/*---------------------------------------------------------------------
Function prototypes.
---------------------------------------------------------------------*/
char 	InPolyhedron( int F, tPointd q, tPointd bmin, tPointd bmax, int radius );
char    SegPlaneInt( tPointi Triangle, tPointd q, tPointd r, tPointd p, int *m );
int     PlaneCoeff( tPointi T, tPointd N, double *D );
void    Assigndi( tPointd p, tPointd a );
void    NormalVec( tPointd q, tPointd b, tPointd c, tPointd N );
double  Dot( tPointd q, tPointd d );
void    SubVec( tPointd q, tPointd b, tPointd c );
char    InTri3D( tPointi T, int m, tPointd p );
char    InTri2D( tPointd Tp[3], tPointd pp );
int     AreaSign( tPointd q, tPointd b, tPointd c );
char    SegTriInt( tPointi Triangle, tPointd q, tPointd r, tPointd p );
char    InPlane( tPointi Triangle, int m, tPointd q, tPointd r, tPointd p);
int     VolumeSign( tPointd a, tPointd b, tPointd c, tPointd d );
char    SegTriCross( tPointi Triangle, tPointd q, tPointd r );
int  	ComputeBox( int F, tPointd bmin, tPointd bmax );
void 	RandomRay( tPointd ray, int radius );
void 	AddVec( tPointd q, tPointd ray );
int  	InBox( tPointd q, tPointd bmin, tPointd bmax );
char 	BoxTest ( int n, tPointd a, tPointd b );
void 	PrintPoint( tPointd q );
int	irint( double x);

/*-------------------------------------------------------------------*/

char inhedron(double bufferX,double bufferY,double bufferZ, int n, int F, int ihDebugFlag)
{
  int i;
  tPointd q, bmin, bmax;
  int radius;
  q[X] = bufferX;
  q[Y] = bufferY;
  q[Z] = bufferZ;

  srandom( (int) time( (long *) 0 ) ); 

  /* Initialize the bounding box */
  for ( i = 0; i < DIM; i++ )
    bmin[i] = bmax[i] = Vertices[0][i];
  radius = ComputeBox( n, bmin, bmax );
//   if(fabs(bufferX-35.7)<1.0e-3) printf("In Inhedron: %d,%f %f %f %f %f %f %f %f %f  %d\n",ihDebugFlag,bufferX,bufferY,bufferZ,bmin[0],bmax[0],bmin[1],bmax[1],bmin[2],bmax[2],radius);
//   fflush(stdout);
  return InPolyhedron( F, q, bmin, bmax, radius ) ;
}

/*
  This function returns a char:
    'V': the query point a coincides with a Vertex of polyhedron P.
    'E': the query point a is in the relative interior of an Edge of polyhedron P.
    'F': the query point a is in the relative interior of a Face of polyhedron P.
    'i': the query point a is strictly interior to polyhedron P.
    'o': the query point a is strictly exterior to( or outside of) polyhedron P.
*/
char InPolyhedron( int F, tPointd q, tPointd bmin, tPointd bmax, int radius )
{
   tPointd r;  /* Ray endpoint. */
   tPointd p;  /* Intersection point; not used. */
   int f, k = 0, crossings = 0;
   char code = '?';
   /* If query point is outside bounding box, finished. */
   if ( !InBox( q, bmin, bmax ) ){
     return 'o';
   }
   LOOP:
   while( k++ < F ) {
      crossings = 0;
  
      RandomRay( r, radius ); 
      AddVec( q, r ); 
     
      for ( f = 0; f < F; f++ ) {  /* Begin check each face */
         if ( BoxTest( f, q, r ) == '0' ) {
              code = '0';
	      // printf("BoxTest = 0!\n");
         }
         else
	   {
	     code = SegTriInt( Faces[f], q, r, p );
	   }
         /* If ray is degenerate, then goto outer while to generate another. */
         if ( code == 'p' || code == 'v' || code == 'e' ) {
            goto LOOP;
         }
         /* If ray hits face at interior point, increment crossings. */
         else if ( code == 'f' ) {
	   crossings++;
         }

         /* If query endpoint q sits on a V/E/F, return that code. */
         else if ( code == 'V' || code == 'E' || code == 'F' )
            return( code );

         /* If ray misses triangle, do nothing. */
         else if ( code == '0' ){
	 }

         else 
            fprintf( stderr, "Error, exit(EXIT_FAILURE)\n" ), exit(1);
      } /* End check each face */

      /* No degeneracies encountered: ray is generic, so finished. */
      break;

   } /* End while loop */
 
   /* q strictly interior to polyhedron iff an odd number of crossings. */
   if( ( crossings % 2 ) == 1 )
      return   'i';
   else{
     return 'o';
   }
}

/*is computing the space diagonal to get the lenght of the ray */
int ComputeBox( int F, tPointd bmin, tPointd bmax )
{
  int i, j;
  double radius;
  
  for( i = 0; i < F; i++ )
    for( j = 0; j < DIM; j++ ) {
      if( Vertices[i][j] < bmin[j] )
	bmin[j] = Vertices[i][j];
      if( Vertices[i][j] > bmax[j] ) 
	bmax[j] = Vertices[i][j];
    }
  
  radius = sqrt( pow( (double)(bmax[X] - bmin[X]), 2.0 ) +
                 pow( (double)(bmax[Y] - bmin[Y]), 2.0 ) +
                 pow( (double)(bmax[Z] - bmin[Z]), 2.0 ) );

  return irint( radius +1 ) + 10;
}

/* Return a random ray endpoint */
void RandomRay( tPointd ray, int radius )
{
  double x, y, z, w, t;

  /* Generate a random point on a sphere of radius 1. */
  /* the sphere is sliced at z, and a random point at angle t
     generated on the circle of intersection. */
  z = 2.0 * (double) random() / MAX_INT - 1.0;
  t = 2.0 * M_PI * (double) random() / MAX_INT;
  w = sqrt( 1 - z*z );
  x = w * cos( t );
  y = w * sin( t );
  
  ray[X] =  radius * x ;
  ray[Y] =  radius * y ;
  ray[Z] =  radius * z ;
  
}

void AddVec( tPointd q, tPointd ray )
{
  int i;
  
  for( i = 0; i < DIM; i++ )
    ray[i] = q[i] + ray[i];
}

int InBox( tPointd q, tPointd bmin, tPointd bmax )
{
  //  int i;

  if( ( bmin[X] <= q[X] ) && ( q[X] <= bmax[X] ) &&
      ( bmin[Y] <= q[Y] ) && ( q[Y] <= bmax[Y] ) &&
      ( bmin[Z] <= q[Z] ) && ( q[Z] <= bmax[Z] ) )
    return TRUE;
  return FALSE;
}
    

/*---------------------------------------------------------------------
    'p': The segment lies wholly within the plane.
    'q': The q endpoint is on the plane (but not 'p').
    'r': The r endpoint is on the plane (but not 'p').
    '0': The segment lies strictly to one side or the other of the plane.
    '1': The segement intersects the plane, and 'p' does not hold.
---------------------------------------------------------------------*/
char	SegPlaneInt( tPointi T, tPointd q, tPointd r, tPointd p, int *m)
{
    tPointd N; double D;
    tPointd rq;
    double num, denom, t;
    int i;

    *m = PlaneCoeff( T, N, &D );
    num = D - Dot( q, N );
    SubVec( r, q, rq );
    denom = Dot( rq, N );
    /*printf("SegPlaneInt: num=%lf, denom=%lf\n", num, denom );*/

    if ( denom >= -e && denom <= e ) {  /* Segment is parallel to plane. */
       if ( num >= -e && num <= e )   /* q is on plane. */
           return 'p';
       else{
	 return '0';
       }
    }

    else
       t = num / denom;

    for( i = 0; i < DIM; i++ )
       p[i] = q[i] + t * ( r[i] - q[i] );

    if ( (0.0 < t) && (t < 1.0) )
         return '1';
    else if ( num >= -e && num <= e  )   /* t == 0 */
         return 'q';
    else if ( num >= denom -e && num <= denom + e  ) /* t == 1 */
         return 'r';
    else{
 return '0';
    }
}
/*---------------------------------------------------------------------
Computes N & D and returns index m of largest component.
---------------------------------------------------------------------*/
int	PlaneCoeff( tPointi T, tPointd N, double *D )
{
    int i;
    double t;              /* Temp storage */
    double biggest = 0.0;  /* Largest component of normal vector. */
    int m = 0;             /* Index of largest component. */

    NormalVec( Vertices[T[0]-1], Vertices[T[1]-1], Vertices[T[2]-1], N );

    *D = Dot( Vertices[T[0]-1], N );

    /* Find the largest component of N. */
    for ( i = 0; i < DIM; i++ ) {
      t = fabs( N[i] );
      if ( t > biggest ) {
        biggest = t;
        m = i;
      }
    }
    return m;
}
/*---------------------------------------------------------------------
Reads in the number and coordinates of the vertices of a polyhedron
from stdin, and returns n, the number of vertices.
---------------------------------------------------------------------*/
/*Obsolete because of superior Nanosculpt treatment  */
/*int 	ReadVertices( void )
{
  int i;
  for(i = 0; i <numVertices ; i++){
    Vertices[i][X] = verticesFile[i].x; 
    Vertices[i][Y] = verticesFile[i].y;
    Vertices[i][Z] = verticesFile[i].z;
  }
   return numVertices;
}
*/
/*---------------------------------------------------------------------
a - b ==> c.
---------------------------------------------------------------------*/
void    SubVec( tPointd a, tPointd b, tPointd c )
{
   int i;

   for( i = 0; i < DIM; i++ )
      c[i] = a[i] - b[i];
}

/*---------------------------------------------------------------------
Returns the dot product of the two input vectors.
---------------------------------------------------------------------*/
double	Dot( tPointd a, tPointd b )
{
    int i;
    double sum = 0.0;

    for( i = 0; i < DIM; i++ )
       sum += a[i] * b[i];

    return  sum;
}

/*---------------------------------------------------------------------
Compute the cross product of (b-a)x(c-a) and place into N.
---------------------------------------------------------------------*/
void	NormalVec( tPointd a, tPointd b, tPointd c, tPointd N )
{
    N[X] = ( c[Z] - a[Z] ) * ( b[Y] - a[Y] ) -
           ( b[Z] - a[Z] ) * ( c[Y] - a[Y] );
    N[Y] = ( b[Z] - a[Z] ) * ( c[X] - a[X] ) -
           ( b[X] - a[X] ) * ( c[Z] - a[Z] );
    N[Z] = ( b[X] - a[X] ) * ( c[Y] - a[Y] ) -
           ( b[Y] - a[Y] ) * ( c[X] - a[X] );
}

/* Assumption: p lies in the plane containing T.
    Returns a char:
     'V': the query point p coincides with a Vertex of triangle T.
     'E': the query point p is in the relative interior of an Edge of triangle T.
     'F': the query point p is in the relative interior of a Face of triangle T.
     '0': the query point p does not intersect (misses) triangle T.
*/

char 	InTri3D( tPointi T, int m, tPointd p )
{
   int i;           /* Index for X,Y,Z           */
   int j;           /* Index for X,Y             */
   int k;           /* Index for triangle vertex */
   tPointd pp;      /* projected p */
   tPointd Tp[3];   /* projected T: three new vertices */

   /* Project out coordinate m in both p and the triangular face */
   j = 0;
   for ( i = 0; i < DIM; i++ ) {
     if ( i != m ) {    /* skip largest coordinate */
       pp[j] = p[i];
       for ( k = 0; k < 3; k++ )
	Tp[k][j] = Vertices[T[k]-1][i];
       j++;
     }
   }
   return( InTri2D( Tp, pp ) );
}

char 	InTri2D( tPointd Tp[3], tPointd pp )
{
   int area0, area1, area2;

   /* compute three AreaSign() values for pp w.r.t. each edge of the face in 2D */
   area0 = AreaSign( pp, Tp[0], Tp[1] );
   area1 = AreaSign( pp, Tp[1], Tp[2] );
   area2 = AreaSign( pp, Tp[2], Tp[0] );
//    printf("area0=%d  area1=%d  area2=%d\n",area0,area1,area2);

   if ( (( area0 == 0 ) && ( area1 > 0 ) && ( area2 > 0 )) ||
        (( area1 == 0 ) && ( area0 > 0 ) && ( area2 > 0 )) ||
        (( area2 == 0 ) && ( area0 > 0 ) && ( area1 > 0 )) ) 
     return 'E';

   if ( (( area0 == 0 ) && ( area1 < 0 ) && ( area2 < 0 )) ||
	(( area1 == 0 ) && ( area0 < 0 ) && ( area2 < 0 )) ||
        (( area2 == 0 ) && ( area0 < 0 ) && ( area1 < 0 )) )
     return 'E';                 
   
   if ( (( area0 >  0 ) && ( area1 > 0 ) && ( area2 > 0 )) ||
        (( area0 <  0 ) && ( area1 < 0 ) && ( area2 < 0 )) )
     return 'F';

   if ( ( area0 == 0 ) && ( area1 == 0 ) && ( area2 == 0 ) )
     fprintf( stderr, "Error in InTriD\n" ), exit(EXIT_FAILURE);

   if ( (( area0 == 0 ) && ( area1 == 0 )) ||
        (( area0 == 0 ) && ( area2 == 0 )) ||
        (( area1 == 0 ) && ( area2 == 0 )) )
     return 'V';

   else {
     //   printf("case e \n"); 
     return '0';  
   }
}

int     AreaSign( tPointd a, tPointd b, tPointd c )  
{
    double area2;

    area2 = ( b[0] - a[0] ) * (double)( c[1] - a[1] ) -
            ( c[0] - a[0] ) * (double)( b[1] - a[1] );

    /* The area should be an integer???. */
    if      ( area2 >  e ) return  1;
    else if ( area2 < (-1*e) ) return -1;
    else                     return  0;
}                            

char    SegTriInt( tPointi T, tPointd q, tPointd r, tPointd p )
{
    int code = '?';
    int m = -1;

    code = SegPlaneInt( T, q, r, p, &m );
  
    if      ( code == '0'){
      //   printf("case f\n");
       return '0';
    }
    else if ( code == 'q')
       return InTri3D( T, m, q );
    else if ( code == 'r')
       return InTri3D( T, m, r );
    else if ( code == 'p' )
       return InPlane( T, m, q, r, p );
    else if ( code == '1' )
       return SegTriCross( T, q, r );
    else /* Error */
       return code;
}

char	InPlane( tPointi T, int m, tPointd q, tPointd r, tPointd p)
{
    /* NOT IMPLEMENTED */
    return 'p';
}

/*---------------------------------------------------------------------
The signed volumes of three tetrahedra are computed, determined
by the segment qr, and each edge of the triangle.  
Returns a char:
   'v': the open segment includes a vertex of T.
   'e': the open segment includes a point in the relative interior of an edge
   of T.
   'f': the open segment includes a point in the relative interior of a face
   of T.
   '0': the open segment does not intersect triangle T.
---------------------------------------------------------------------*/

char SegTriCross( tPointi T, tPointd q, tPointd r )
{
   int vol0, vol1, vol2;
   
   vol0 = VolumeSign( q, Vertices[ T[0]-1 ], Vertices[ T[1]-1 ], r ); 
   vol1 = VolumeSign( q, Vertices[ T[1]-1 ], Vertices[ T[2]-1 ], r ); 
   vol2 = VolumeSign( q, Vertices[ T[2]-1 ], Vertices[ T[0]-1 ], r );
 
   //  printf( "SegTriCross:  vol0 = %d; vol1 = %d; vol2 = %d\n", vol0, vol1, vol2 ); 
     
   /* Same sign: segment intersects interior of triangle. */
   if ( ( ( vol0 > 0 ) && ( vol1 > 0 ) && ( vol2 > 0 ) ) || 
        ( ( vol0 < 0 ) && ( vol1 < 0 ) && ( vol2 < 0 ) ) )
     {
      return 'f';
     }
   
   /* Opposite sign: no intersection between segment and triangle */
   if ( ( ( vol0 > 0 ) || ( vol1 > 0 ) || ( vol2 > 0 ) ) &&
        ( ( vol0 < 0 ) || ( vol1 < 0 ) || ( vol2 < 0 ) ) ){
     //  printf("case g\n");
      return '0';
   }

   else if ( ( vol0 == 0 ) && ( vol1 == 0 ) && ( vol2 == 0 ) )
     fprintf( stderr, "Error 1 in SegTriCross\n" ), exit(EXIT_FAILURE);
   
   /* Two zeros: segment intersects vertex. */
   else if ( ( ( vol0 == 0 ) && ( vol1 == 0 ) ) || 
             ( ( vol0 == 0 ) && ( vol2 == 0 ) ) || 
             ( ( vol1 == 0 ) && ( vol2 == 0 ) ) )
      return 'v';

   /* One zero: segment intersects edge. */
   else if ( ( vol0 == 0 ) || ( vol1 == 0 ) || ( vol2 == 0 ) )
      return 'e';
   
   else
     fprintf( stderr, "Error 2 in SegTriCross\n" ), exit(EXIT_FAILURE);
}

int 	VolumeSign( tPointd a, tPointd b, tPointd c, tPointd d )
{ 
   double vol;
   double ax, ay, az, bx, by, bz, cx, cy, cz, dx, dy, dz;
   double bxdx, bydy, bzdz, cxdx, cydy, czdz;

   ax = a[X];
   ay = a[Y];
   az = a[Z];
   bx = b[X];
   by = b[Y];
   bz = b[Z];
   cx = c[X]; 
   cy = c[Y];
   cz = c[Z];
   dx = d[X];
   dy = d[Y];
   dz = d[Z];

   bxdx=bx-dx;
   bydy=by-dy;
   bzdz=bz-dz;
   cxdx=cx-dx;
   cydy=cy-dy;
   czdz=cz-dz;
   vol =   (az-dz) * (bxdx*cydy - bydy*cxdx)
         + (ay-dy) * (bzdz*cxdx - bxdx*czdz)
         + (ax-dx) * (bydy*czdz - bzdz*cydy);


   /* The volume should be an integer. */
   if      ( vol > e )   return  1;
   else if ( vol < (-1*e) )  return -1;
   else                    return  0;
}

/*
  This function returns a char:
    '0': the segment [ab] does not intersect (completely misses) the 
         bounding box surrounding the n-th triangle T.  It lies
         strictly to one side of one of the six supporting planes.
    '?': status unknown: the segment may or may not intersect T.
*/
char BoxTest ( int n, tPointd a, tPointd b )
{
   int i; /* Coordinate index */
   double w;

   for ( i=0; i < DIM; i++ ) {
       w = Box[n][0][i]; /* min: lower left */
       if ( (a[i] < w) && (b[i] < w) ) return '0';
       w = Box[n][1][i]; /* max: upper right */
       if ( (a[i] > w) && (b[i] > w) ) return '0';
   }
   return '?';
}

/* irint not available in some libraries, so... */

int	irint( double x )
{
	return (int) rint( x );
}
