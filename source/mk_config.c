//***************************************************************************************************************************
// nanoSCULPT: A tool to generate complex and realistic configurations for atomistic simulations
// Copyright (C) 2011-2015
// @authors: A. Prakash, M. Hummel, S. Schmauder and E. Bitzek, 
//
//
/* Parts of the code come from LEGO
 * LEGO (Lattice Elemental Geometry Operations)
 * Program to create arbitary rotated and cut FCC structures in IMD format 
 * Erik Bitzek Jan 2000
 */ 
//
//
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 * 
 */
/* Time messurement from IMD 
 * Martin Hummel November 2011 
 * Arun - corrected the calculation of lower and upper limits for the box
 * Arun - added capabilities of L12, B2 and D011 structures from new code of lego
 * Arun - code for B2, D011 not yet activated
 * Arun - made changes to the calculation of multipliers for filling up atoms
 *      - it is now done as in LEGO; I am using the max multiplier in all directions!
 */
//***************************************************************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "mk_config_globals.h"
#define EPS 1.0e-4         /* epsilon for determination of supercell */
#define MAX_SCELL 100000000000000.0    /* max. supercell dimension               */
#define DIM 3

/* Function Prototypes */
void getparams(char *inputfilename);
void mvmult(double matrix[3][3],vektor v, vektor *res);
void creatematrix(double matrix[3][3], vektor achse, double winkel);
double vabs(vektor a);
double smult(vektor a, vektor b);
void cross(vektor a,vektor b, vektor *res);
double det(double matrix[3][3]);
void readData(char *inputfilename);
void minbox(void);
int main (int argc, char *argv[]);
int irint(double);
char inhedron(double bufferX,double bufferY,double bufferZ, int n, int F, int ihDebugFlag);
void triangulateMain();
void simpleTriangulation(void);
void     ReadVertices( void );
void     ReadFaces( void );
int PolyederVertices(void);
void outputHeader(void);

int main(int argc, char *argv[])
{
  clock_t starttime, endtime;
  double cpu_time_used;
  double max_xcomponent,max_ycomponent,max_zcomponent;
  int i,j,k;
  vektor p,pDebug;
  int nr;
  int testetAtoms = 0;
  int maxPolVert;
  int lxlim,lylim,lzlim;
  int rxlim,rylim,rzlim;
  int ihDebugFlag=0;
  int sfac; //scaling fac for the trapezoidal box (of basis vectors) of atoms
  
  //For debug purposes only
  str255 outdebugfile;
  FILE *outdebug;
  
  // tmp variables for output of L12,B2,... structures
  int atom_type=0;   
  double atom_mass=0.0;
  vektor tp;
  
//  double to_ps=1;
  double to_ps=0.000103650; // Conversion factor for converting mass to IMD units

  starttime = clock();
  
  int hasbasis =0;
  
  // Output software and copyright info
  // TODO: pass version number as argument to function
  outputHeader();
  
  /* get global parameters */
  getparams(argv[1]);
  //    printf("After getparams\n");
  
  // Read input surface mesh
  readData(eingabe);
  
  /* Information about input values */
  printf("-----Input values-----\n");
  printf("Crystalstructure: ---------------> %s\n",struktur);
  printf("Epsilon: ------------------------> %f\n",e);
  printf("Input surface mesh file: --------> %s\n",eingabe);
  printf("Output Structure file: ----------> %s\n",outfile);
  printf("Atomnr. starting value: ---------> %d\n",start);
  printf("Atomic mass: --------------------> %f\n",m);
  printf("Lattice constant: ---------------> %f\n",a);
  printf("Center_shift: -------------------> %f %f %f\n",center_shift.x,center_shift.y,center_shift.z);
  if(idebug==1){
    printf("Debug Flag: ---------------------> %d\n",idebug);
  }
  printf("---\n");
  printf("Orientation used: --------------->\n");
  printf("%f  %f  %f\n",coordSystem[0][0],coordSystem[0][1],coordSystem[0][2]);
  printf("%f  %f  %f\n",coordSystem[1][0],coordSystem[1][1],coordSystem[1][2]);
  printf("%f  %f  %f\n",coordSystem[2][0],coordSystem[2][1],coordSystem[2][2]);
  printf("---\n\n");
  //   printf("mass (rescaled): %f\n",m);
  if(custom_box==1){
    printf("!!!Using Custom box limits!!!\n");
  }
  
  
  minbox();
  printf("BoxMinimal: %f \t %f \t %f \t\n",bminimal.x, bminimal.y, bminimal.z);
  printf("BoxMaximal: %f \t %f \t %f \t\n",bmaximal.x, bmaximal.y, bmaximal.z);
//   printf("obj File: %s \n",eingabe);
//   printf("Point in Polyhedron test char: %s \n",pipchar);
  
  
  // Box and header write  information
  printf("\nHeader write flag (for output file): %d\n",header);
  printf("box_x: %f %f %f\n",box_x.x,box_x.y,box_x.z);
  printf("box_y: %f %f %f\n",box_y.x,box_y.y,box_y.z);
  printf("box_z: %f %f %f\n",box_z.x,box_z.y,box_z.z);
  printf("\n");
  
  
  
  /* (rotated) translation vectors */
  vektor a1,a2,a3;
  vektor a1_rot INIT(nullvektor);
  vektor a2_rot INIT(nullvektor);
  vektor a3_rot INIT(nullvektor);
//   vektor a1_rot2 INIT(nullvektor);
//   vektor a2_rot2 INIT(nullvektor);
//   vektor a3_rot2 INIT(nullvektor);
  
  /* (rotated) basis vectors */
  vektor b1,b2,b3,b4;
//   vector b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16;
  vektor b1_rot INIT(nullvektor);
  vektor b2_rot INIT(nullvektor);
  vektor b3_rot INIT(nullvektor);
  vektor b4_rot INIT(nullvektor);
//   vektor b5_rot INIT(nullvektor);
//   vektor b6_rot INIT(nullvektor);
//   vektor b7_rot INIT(nullvektor);
//   vektor b8_rot INIT(nullvektor);
//   vektor b9_rot INIT(nullvektor);
//   vektor b10_rot INIT(nullvektor);
//   vektor b11_rot INIT(nullvektor);
//   vektor b12_rot INIT(nullvektor);
//   vektor b13_rot INIT(nullvektor);
//   vektor b14_rot INIT(nullvektor);
//   vektor b15_rot INIT(nullvektor);
//   vektor b16_rot INIT(nullvektor);
//   vektor b1_rot2 INIT(nullvektor);
//   vektor b2_rot2 INIT(nullvektor);
//   vektor b3_rot2 INIT(nullvektor);
//   vektor b4_rot2 INIT(nullvektor);
//   vektor b5_rot2 INIT(nullvektor);
//   vektor b6_rot2 INIT(nullvektor);
//   vektor b7_rot2 INIT(nullvektor);
//   vektor b8_rot2 INIT(nullvektor);
//   vektor b9_rot2 INIT(nullvektor);
//   vektor b10_rot2 INIT(nullvektor);
//   vektor b11_rot2 INIT(nullvektor);
//   vektor b12_rot2 INIT(nullvektor);
//   vektor b13_rot2 INIT(nullvektor);
//   vektor b14_rot2 INIT(nullvektor);
//   vektor b15_rot2 INIT(nullvektor);
//   vektor b16_rot2 INIT(nullvektor);
  
  
  /* (rotated) standard vectors */
  vektor e1,e2,e3;
  vektor e1_rot INIT(nullvektor);
  vektor e2_rot INIT(nullvektor);
  vektor e3_rot INIT(nullvektor);
//   vektor e1_rot2 INIT(nullvektor);
//   vektor e2_rot2 INIT(nullvektor);
//   vektor e3_rot2 INIT(nullvektor);
  
  e1.x = 1.0;
  e1.y = 0;
  e1.z = 0;

  e2.x = 0.0;
  e2.y = 1.0;
  e2.z = 0;
      
  e3.x = 0.0;
  e3.y = 0.0;
  e3.z = 1.0;
  
  /* default values */
  type=0;
  c=0.0;
 
  drehmatrix1[0][0] = 1.0;
  drehmatrix1[0][1] = 0.0;
  drehmatrix1[0][2] = 0.0;
  drehmatrix1[1][0] = 0.0;
  drehmatrix1[1][1] = 1.0;
  drehmatrix1[1][2] = 0.0;
  drehmatrix1[2][0] = 0.0;
  drehmatrix1[2][1] = 0.0;
  drehmatrix1[2][2] = 1.0;
   
  
  /*checking whether only triangles or other polygons border the body */
  maxPolVert = PolyederVertices();
//   printf("maxPolVert: %d\n",maxPolVert);
  if (maxPolVert > 3){
    simpleTriangulation();
  }
  else if (maxPolVert < 3){
    printf("faces with %i\t vertices\n", maxPolVert);
  }
  else {
    for(i = 0 ; i < numPolygon;i++){
      triangles[i].x= polygon[i][0];
      triangles[i].y= polygon[i][1];
      triangles[i].z= polygon[i][2];
      trianglesNumber = numPolygon;
    }
  }
  
  //   printf(" %i\t triangles out of %i\t polygons\n", trianglesNumber, numPolygon);
  ReadVertices();//generalized reading for inhedron vertices
//   printf(" Finished reading vertices !! \n");
  ReadFaces(); //generalized reading for inhedron triangles
//   printf(" Finished reading faces!! \n\n");
//   printf("Crystalstructure: %s\n",struktur);
  
  /*submitting the read in roation matrix */
  drehmatrix1[0][0] = coordSystem[0][0];
  drehmatrix1[0][1] = coordSystem[0][1];
  drehmatrix1[0][2] = coordSystem[0][2];
  drehmatrix1[1][0] = coordSystem[1][0];
  drehmatrix1[1][1] = coordSystem[1][1];
  drehmatrix1[1][2] = coordSystem[1][2];
  drehmatrix1[2][0] = coordSystem[2][0];
  drehmatrix1[2][1] = coordSystem[2][1];
  drehmatrix1[2][2] = coordSystem[2][2];
 
/* Ergänzungen Ende */
  
  /* preparations for output */
  m *= to_ps;
  nr = start;
  
  /* open outfile */
  out = fopen(outfile,"w");
  if (NULL==out){
    printf("Can't open outputfile %s\n",outfile);
    exit(2);
  }
  // IMD format gets first class preferential treatment!!
  if (header==1){
    fprintf(out,"#F A 1 1 1 3 0 0\n#C number type mass x y z\n");
    fprintf(out,"#X %f %f %f\n",box_x.x,box_x.y,box_x.z);
    fprintf(out,"#Y %f %f %f\n",box_y.x,box_y.y,box_y.z);
    fprintf(out,"#Z %f %f %f\n",box_z.x,box_z.y,box_z.z);
    fprintf(out,"## generated by nanoSCULPT version 11.06.2014\n");
    fprintf(out,"## Authors: Arun Prakash and Erik Bitzek\n");
    fprintf(out,"#E\n");
  }
  

  if (strcasecmp(struktur,"sc")==0)
    {
      /* primitve translations of fcc */
        // see Ashcroft
      a1.x = a;       
      a1.y = 0.0;       
      a1.z = 0.0;

      a2.x = 0.0;
      a2.y = a;
      a2.z = 0.0;
      
      a3.x = 0.0;
      a3.y = 0.0;
      a3.z = a;
    }
  
  else if (strcasecmp(struktur,"fcc")==0)
      // diamond = 2*fcc, w. shiftvector a1/4 +a2/4+a3/4
    {
      /* primitve translations of fcc */
        // see Ashcroft
      a1.x = 0.5*a;       
      a1.y = 0.5*a;       
      a1.z = 0.0;

      a2.x = 0.0;
      a2.y = 0.5*a;
      a2.z = 0.5*a;
      
      a3.x = 0.5*a;
      a3.y = 0.0;
      a3.z = 0.5*a;
    }
  else if (strcasecmp(struktur,"bcc")==0)
    {
    /* primitve translations of bcc */
      a1.x = 0.5*a;       
      a1.y = 0.5*a;       
      a1.z =-0.5*a;

      a2.x =-0.5*a;
      a2.y = 0.5*a;
      a2.z = 0.5*a;
      
      a3.x = 0.5*a;
      a3.y =-0.5*a;
      a3.z = 0.5*a;
    }
  else if (strcasecmp(struktur,"hex")==0)
      // hcp = 2*hex, w. shiftvector a1/3 +a2/3+a3/2
  {
      if (c<=0)
      {
          printf("Hey, %s needs more lattice parameters, but c=%f\n",struktur,c);
          exit(2);
      }

      /* primitve translations of hcp */
      a1.x = 1.0*a;       
      a1.y = 0.0;
      a1.z = 0.0;

      a2.x = 0.5*a;
      a2.y = sqrt(3.0)*a/2.0;
      a2.z = 0.0;
      
      a3.x = 0.0;
      a3.y = 0.0;
      a3.z = c;
    }
    else if (strcasecmp(struktur,"L12")==0)
      // e.g. Ni3Al, now we use for the first time basis vectors ;-)
    {
      /* primitive vectors */
      // see http://cst-www.nrl.navy.mil/lattice/struk/l1_2.html
      a1.x = 1.0*a;       
      a1.y = 0.0;       
      a1.z = 0.0;
      
      a2.x = 0.0;
      a2.y = 1.0*a;
      a2.z = 0.0;
      
      a3.x = 0.0;
      a3.y = 0.0;
      a3.z = 1.0*a;
      
      m1 *=to_ps;
      printf("structure with 2 atoms:\n 0: mass %f amu =%f 1: mass %f amu=%f \n",m,m/to_ps,m1,m1/to_ps);
      
      hasbasis =1;
      nbasisvects=4;
      
      // Al
      b1.x = 0.0;       
      b1.y = 0.0;       
      b1.z = 0.0;
      basistype[1]=1; // same numbering as for basisvector
      
      // Ni
      
      b2.x = 0.0;
      b2.y = 0.5*a;
      b2.z = 0.5*a;
      basistype[2]=0;
      
      b3.x = 0.5*a;
      b3.y = 0.0;
      b3.z = 0.5*a;
      basistype[3]=0;
      
      b4.x = 0.5*a;
      b4.y = 0.5*a;
      b4.z = 0.0;
      basistype[4]=0;
      
    }
//     else if (strcasecmp(struktur,"B2")==0)
//       // e.g. NiAl
//     // adapted from L12 and bcc structure
//     {
//       /* primitive vectors */
//       // see http://cst-www.nrl.navy.mil/lattice/struk/l1_2.html
//       a1.x = 1.0*a;       
//       a1.y = 0.0;       
//       a1.z = 0.0;
//       
//       a2.x = 0.0;
//       a2.y = 1.0*a;
//       a2.z = 0.0;
//       
//       a3.x = 0.0;
//       a3.y = 0.0;
//       a3.z = 1.0*a;
//       
//       m1 *=to_ps;
//       printf("structure with 2 atoms:\n 0: mass %f amu =%f 1: mass %f amu=%f \n",m,m/to_ps,m1,m1/to_ps);
//       
//       hasbasis =2;
//       nbasisvects=2;
//       
//       // Al
//       b1.x = 0.0;       
//       b1.y = 0.0;       
//       b1.z = 0.0;
//       basistype[1]=0; // same numbering as for basisvector
//       
//       // Ni
//       
//       b2.x = 0.5*a;
//       b2.y = 0.5*a;
//       b2.z = 0.5*a;
//       basistype[2]=1;
//       
//       
//       /* primitve translations of bcc 
//        *      a1.x = 0.5*a;
//        *      a1.y = 0.5*a;
//        *      a1.z =-0.5*a;
//        * 
//        *      a2.x =-0.5*a;
//        *      a2.y = 0.5*a;
//        *      a2.z = 0.5*a;
//        * 
//        *      a3.x = 0.5*a;
//        *      a3.y =-0.5*a;
//        *      a3.z = 0.5*a;
//        */
//       
//     }
//     else if (strcasecmp(struktur,"DO11")==0)
//       // e.g. Fe3C
//     // implemented by Jo Moeller, Sun Jan 27 11:40:02 CET 2013
//     // 
//     {
//       /* primitive vectors */
//       // see http://cst-www.nrl.navy.mil/lattice/struk/l1_2.html
//       a1.x = 1.0*a;       
//       a1.y = 0.0;       
//       a1.z = 0.0;
//       
//       a2.x = 0.0;
//       a2.y = 1.0*b;
//       a2.z = 0.0;
//       
//       a3.x = 0.0;
//       a3.y = 0.0;
//       a3.z = 1.0*c;
//       
//       m1 *=to_ps;
//       printf("structure with 2 atoms:\n 0: mass %f amu =%f 1: mass %f amu=%f \n",m,m/to_ps,m1,m1/to_ps);
//       printf("initial Wyckoff positions:\n 8d(0): %f, %f, %f \n 4c(0): %f, %f, %f \n 4c(1): %f, %f, %f \n",i8d.x,i8d.y,i8d.z,i4c.x,i4c.y,i4c.z,i4c1.x,i4c1.y,i4c1.z);
//       
//       hasbasis=3;
//       nbasisvects=16;
//       
//       // the 8 general base vectors of atom type 0, e.g., FeI:
//       b1.x = i8d.x*a;       
//       b1.y = i8d.y*b;       
//       b1.z = i8d.z*c;
//       basistype[1]=0;
//       
//       b2.x = (1-i8d.x+0.5)*a;
//       b2.y = (1-i8d.y)*b;
//       b2.z = (i8d.z+0.5)*c;
//       basistype[2]=0;
//       
//       b3.x = (1-i8d.x)*a;
//       b3.y = (i8d.y+0.5)*b;
//       b3.z = (1-i8d.z)*c;
//       basistype[3]=0;
//       
//       b4.x = (i8d.x+0.5)*a;
//       b4.y = (1-i8d.y+0.5)*b;
//       b4.z = (1-i8d.z+0.5)*c;
//       basistype[4]=0;
//       
//       b5.x = (1-i8d.x)*a;
//       b5.y = (1-i8d.y)*b;
//       b5.z = (1-i8d.z)*c;
//       basistype[5]=0;
//       
//       b6.x = (i8d.x+0.5)*a;
//       b6.y = (i8d.y)*b;
//       b6.z = (1-i8d.z+0.5)*c;
//       basistype[6]=0;
//       
//       b7.x = i8d.x*a;
//       b7.y = (1-i8d.y+0.5)*b;
//       b7.z = i8d.z*c;
//       basistype[7]=0;
//       
//       b8.x = (-i8d.x+0.5)*a;
//       b8.y = (i8d.y+0.5)*b;
//       b8.z = (i8d.z+0.5)*c;
//       basistype[8]=0;
//       
//       // the 4 special base vectors of atom type 0, e.g., FeII:
//       b9.x = i4c.x*a;
//       b9.y = i4c.y*b;
//       b9.z = i4c.z*c;
//       basistype[9]=0;
//       
//       b10.x = (-i4c.x+0.5)*a;
//       b10.y = (1-i4c.y)*b;
//       b10.z = (i4c.z+0.5)*c;
//       basistype[10]=0;
//       
//       b11.x = (1-i4c.x)*a;       
//       b11.y = (1-i4c.y)*b;       
//       b11.z = (1-i4c.z)*c;
//       basistype[11]=0;
//       
//       b12.x = (i4c.x+0.5)*a;
//       b12.y = (i4c.y)*b;
//       b12.z = (-i4c.z+0.5)*c;
//       basistype[12]=0;
//       
//       // the 4 special base vectors of atom type 1, e.g., C:
//       b13.x = i4c1.x*a;
//       b13.y = i4c1.y*b;
//       b13.z = i4c1.z*c;
//       basistype[13]=1;
//       
//       b14.x = (-i4c1.x+0.5)*a;
//       b14.y = (1-i4c1.y)*b;
//       b14.z = (i4c1.z+0.5)*c;
//       basistype[14]=1;
//       
//       b15.x = (1-i4c1.x)*a;       
//       b15.y = (1-i4c1.y)*b;       
//       b15.z = (1-i4c1.z)*c;
//       basistype[15]=1;
//       
//       b16.x = (i4c1.x+0.5)*a;
//       b16.y = (i4c1.y)*b;
//       b16.z = (-i4c1.z+0.5)*c;
//       basistype[16]=1;
//       
//     }
//     
  else 
    {
      printf("Unknown Crystalstructure : %s\n",struktur);
      exit(2);
    }

 
  if(fabs(det(drehmatrix1)-1.0) > EPS)
    {
      printf("Det(drehmatrix1)=%f\n",det(drehmatrix1));
      exit(2);
    }

  mvmult(drehmatrix1,a1,&a1_rot);
  mvmult(drehmatrix1,a2,&a2_rot);
  mvmult(drehmatrix1,a3,&a3_rot);
  
  printf("unit vectors after 1. Rotation:\n");
  printf(" %.8f %.8f %.8f\n",a1_rot.x,a1_rot.y,a1_rot.z);
  printf(" %.8f %.8f %.8f\n",a2_rot.x,a2_rot.y,a2_rot.z);
  printf(" %.8f %.8f %.8f\n\n",a3_rot.x,a3_rot.y,a3_rot.z);
  
  /* L12 */
  if (hasbasis==1)
  {
//     printf("basis vectors before 1. Rotation:\n");
//     printf("type %d: %.8f %.8f %.8f\n",  basistype[1],b1.x,b1_rot2.y,b1_rot2.z);
//     printf("type %d: %.8f %.8f %.8f\n",  basistype[2],b2_rot2.x,b2_rot2.y,b2_rot2.z);
//     printf("type %d: %.8f %.8f %.8f\n",  basistype[3],b3_rot2.x,b3_rot2.y,b3_rot2.z);
//     printf("type %d: %.8f %.8f %.8f\n\n",basistype[4],b4_rot2.x,b4_rot2.y,b4_rot2.z);
    mvmult(drehmatrix1,b1,&b1_rot);
    mvmult(drehmatrix1,b2,&b2_rot);
    mvmult(drehmatrix1,b3,&b3_rot);
    mvmult(drehmatrix1,b4,&b4_rot);
    printf("basis vectors after 1. Rotation:\n");
    printf("type %d: %.8f %.8f %.8f\n",  basistype[1],b1_rot.x,b1_rot.y,b1_rot.z);
    printf("type %d: %.8f %.8f %.8f\n",  basistype[2],b2_rot.x,b2_rot.y,b2_rot.z);
    printf("type %d: %.8f %.8f %.8f\n",  basistype[3],b3_rot.x,b3_rot.y,b3_rot.z);
    printf("type %d: %.8f %.8f %.8f\n\n",basistype[4],b4_rot.x,b4_rot.y,b4_rot.z);
  }
  
  mvmult(drehmatrix1,e1,&e1_rot);
  mvmult(drehmatrix1,e2,&e2_rot);
  mvmult(drehmatrix1,e3,&e3_rot);
  
  if(custom_box==1){
    bminimal=box_minimal;
    bmaximal=box_maximal;
  }
  boxCenter.x = 0.5*( bminimal.x + bmaximal.x) +center_shift.x;
  boxCenter.y = 0.5*( bminimal.y + bmaximal.y) +center_shift.y;
  boxCenter.z = 0.5*( bminimal.z + bmaximal.z) +center_shift.z;
//   printf("BoxMinimal: %f \t %f \t %f \t\n",bminimal.x, bminimal.y, bminimal.z);
//   printf("BoxMaximal: %f \t %f \t %f \t\n",bmaximal.x, bmaximal.y, bmaximal.z);
  printf("Box centers: %f, %f, %f\n\n",boxCenter.x,boxCenter.y,boxCenter.z);

//   if(abs(a1_rot.x)>=abs(a2_rot.x))max_xcomponent=abs(a1_rot.x);
//   if(abs(a1_rot.x)<abs(a2_rot.x))max_xcomponent=abs(a2_rot.x);
//   if(max_xcomponent<abs(a3_rot.x))max_xcomponent=abs(a3_rot.x);
  max_xcomponent=(fabs(a1_rot.x)>=fabs(a2_rot.x))?fabs(a1_rot.x):fabs(a2_rot.x);
  max_xcomponent=( max_xcomponent>=fabs(a3_rot.x))? max_xcomponent:fabs(a3_rot.x);
  max_ycomponent=(fabs(a1_rot.y)>=fabs(a2_rot.y))?fabs(a1_rot.y):fabs(a2_rot.y);
  max_ycomponent=( max_ycomponent>=fabs(a3_rot.y))? max_ycomponent:fabs(a3_rot.y);
  max_zcomponent=(fabs(a1_rot.z)>=fabs(a2_rot.z))?fabs(a1_rot.z):fabs(a2_rot.z);
  max_zcomponent=( max_zcomponent>=fabs(a3_rot.z))? max_zcomponent:fabs(a3_rot.z);
  if(idebug==1)printf("max_component: %f %f %f\n\n",max_xcomponent,max_ycomponent,max_zcomponent);
//   printf("a: %f %f %f\n",fabs(a1_rot.x),a2_rot.y,a3_rot.z);
//   printf("irint --> %f, %d, %d\n",(bminimal.x - boxCenter.x)/max_xcomponent,irint((bminimal.x - boxCenter.x)/max_xcomponent),abs(irint((bminimal.x - boxCenter.x)/max_xcomponent)) +10);
  
/*   lower.x = -1*(abs(irint((bminimal.x - boxCenter.x)/a1_rot.x)) +10); */
/*   lower.y = -1*(abs(irint((bminimal.y - boxCenter.y)/a2_rot.y)) +10); */
/*   lower.z = -1*(abs(irint((bminimal.z - boxCenter.z)/a3_rot.z)) +10); */

  if(idebug==1)printf("bminimal-boxcenter\n");
  if(idebug==1)printf("%f %f %f \n",(bminimal.x-boxCenter.x),(bminimal.y-boxCenter.y),(bminimal.z-boxCenter.z));
  if(idebug==1)printf("bminimal-bmaximal\n");
  if(idebug==1)printf("%f %f %f \n",(bminimal.x-bmaximal.x),(bminimal.y-bmaximal.y),(bminimal.z-bmaximal.z));
  if(idebug==1)printf("(bminimal-bmaximal)/max_component\n");
  if(idebug==1)printf("%f %f %f \n",(bminimal.x-boxCenter.x)/max_xcomponent,(bminimal.y-boxCenter.y)/max_ycomponent,(bminimal.z-boxCenter.z)/max_zcomponent);
  if(idebug==1)printf("irint((bminimal-bmaximal)/max_component)\n");
  if(idebug==1)printf("%d %d %d \n",irint((bminimal.x-boxCenter.x)/max_xcomponent),irint((bminimal.y-boxCenter.y)/max_ycomponent),irint((bminimal.z-boxCenter.z)/max_zcomponent));
  
    
  lower.x = -1*(abs(irint((bminimal.x - boxCenter.x)/max_xcomponent)) +10);
  lower.y = -1*(abs(irint((bminimal.y - boxCenter.y)/max_ycomponent)) +10);
  lower.z = -1*(abs(irint((bminimal.z - boxCenter.z)/max_zcomponent)) +10);
//   lower.x = -1*(abs(irint((bminimal.x - bmaximal.x)/max_xcomponent)) +10);
//   lower.y = -1*(abs(irint((bminimal.y - bmaximal.y)/max_ycomponent)) +10);
//   lower.z = -1*(abs(irint((bminimal.z - bmaximal.z)/max_zcomponent)) +10);
  
  if(idebug==1)printf("lower x: %d upper x: %d\n",lower.x,-lower.x);
  if(idebug==1)printf("lower y: %d upper y: %d\n",lower.y,-lower.y);
  if(idebug==1)printf("lower z: %d upper z: %d\n",lower.z,-lower.z);

  /* calculate atom positions */
  if(idebug==1)printf("bminimal = %f \t %f \t %f \t\n", bminimal.x,bminimal.y,bminimal.z);
  if(idebug==1)printf("bmaximal = %f \t %f \t %f \t\n", bmaximal.x,bmaximal.y,bmaximal.z);
  
//   lxlim=2*lower.x; lylim=2*lower.y; lzlim=2*lower.z;
//   rxlim=-2*lower.x; rylim=-2*lower.y; rzlim=-2*lower.z;


  // Alternate plan 1: Consider the max of all limits!!
  // NOTE: lower.x, lower.y and lower.z as calculated above are all negative!!!
  lxlim=(lower.x<=lower.y)?lower.x:lower.y;  lxlim=(lxlim<=lower.z)?lxlim:lower.z;
  sfac=1.0; //scaling fac for the trapezoidal box of atoms
  lxlim=sfac*lxlim; lylim=lxlim; lzlim=lxlim;
  rxlim=-1*lxlim; rylim=rxlim; rzlim=rxlim;
  if(idebug==1)printf("Loop limits: %d %d; %d %d; %d %d\n",lxlim,rxlim,lylim,rylim,lzlim,rzlim);


  
  // For debug purposes only
//   printf("Coordinates of the cuboid! \n");
  if(idebug==1){
    strcpy(outdebugfile,eingabe);
    strcat(outdebugfile,".nanoSCULPToutput.obj");
    printf("\nWriting Coordinates of the Cuboid in file: %s\n",outdebugfile);
    outdebug=fopen(outdebugfile,"w");
    if (NULL==outdebug){
      printf("Can't open debug outputfile %s\n",outdebugfile);
      exit(5);
    }
    fprintf(outdebug,"#Cuboidal coordinates calculated inside of nanoSCULPT\n");
    fprintf(outdebug,"#Original 3D volume file --> %s\n",eingabe);
    i=lxlim; j=lylim; k=lzlim;
    pDebug.x=i*a1_rot.x +  j*a2_rot.x + k*a3_rot.x;
    pDebug.y=i*a1_rot.y +  j*a2_rot.y + k*a3_rot.y;
    pDebug.z=i*a1_rot.z +  j*a2_rot.z + k*a3_rot.z;
    fprintf(outdebug,"v %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    if(idebug==1)printf("p of -x,-y,-z: %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);

    i=rxlim; j=lylim; k=lzlim;
    pDebug.x=i*a1_rot.x +  j*a2_rot.x + k*a3_rot.x;
    pDebug.y=i*a1_rot.y +  j*a2_rot.y + k*a3_rot.y;
    pDebug.z=i*a1_rot.z +  j*a2_rot.z + k*a3_rot.z;
    fprintf(outdebug,"v %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    if(idebug==1)printf("p of +x,+y,-z: %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    
    i=rxlim; j=rylim; k=lzlim;
    pDebug.x=i*a1_rot.x +  j*a2_rot.x + k*a3_rot.x;
    pDebug.y=i*a1_rot.y +  j*a2_rot.y + k*a3_rot.y;
    pDebug.z=i*a1_rot.z +  j*a2_rot.z + k*a3_rot.z;
    fprintf(outdebug,"v %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    if(idebug==1)printf("p of +x,-y,-z: %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);

    i=lxlim; j=rylim; k=lzlim;
    pDebug.x=i*a1_rot.x +  j*a2_rot.x + k*a3_rot.x;
    pDebug.y=i*a1_rot.y +  j*a2_rot.y + k*a3_rot.y;
    pDebug.z=i*a1_rot.z +  j*a2_rot.z + k*a3_rot.z;
    fprintf(outdebug,"v %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    if(idebug==1)printf("p of -x,+y,-z: %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    
    i=lxlim; j=lylim; k=rzlim;
    pDebug.x=i*a1_rot.x +  j*a2_rot.x + k*a3_rot.x;
    pDebug.y=i*a1_rot.y +  j*a2_rot.y + k*a3_rot.y;
    pDebug.z=i*a1_rot.z +  j*a2_rot.z + k*a3_rot.z;
    fprintf(outdebug,"v %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    if(idebug==1)printf("p of -x,-y,+z: %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    
    i=rxlim; j=lylim; k=rzlim;
    pDebug.x=i*a1_rot.x +  j*a2_rot.x + k*a3_rot.x;
    pDebug.y=i*a1_rot.y +  j*a2_rot.y + k*a3_rot.y;
    pDebug.z=i*a1_rot.z +  j*a2_rot.z + k*a3_rot.z;
    fprintf(outdebug,"v %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    if(idebug==1)printf("p of +x,-y,+z: %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    
    i=rxlim; j=rylim; k=rzlim;
    pDebug.x=i*a1_rot.x +  j*a2_rot.x + k*a3_rot.x;
    pDebug.y=i*a1_rot.y +  j*a2_rot.y + k*a3_rot.y;
    pDebug.z=i*a1_rot.z +  j*a2_rot.z + k*a3_rot.z;
    fprintf(outdebug,"v %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    if(idebug==1)printf("p of +x,+y,+z: %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    
    i=lxlim; j=rylim; k=rzlim;
    pDebug.x=i*a1_rot.x +  j*a2_rot.x + k*a3_rot.x;
    pDebug.y=i*a1_rot.y +  j*a2_rot.y + k*a3_rot.y;
    pDebug.z=i*a1_rot.z +  j*a2_rot.z + k*a3_rot.z;
    fprintf(outdebug,"v %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    if(idebug==1)printf("p of -x,+y,+z: %f %f %f\n",pDebug.x+boxCenter.x,pDebug.y+boxCenter.y,pDebug.z+boxCenter.z);
    
    fprintf(outdebug,"f 1 2 3 4\n");
    fprintf(outdebug,"f 5 6 7 8\n");
    fprintf(outdebug,"f 1 5 8 4\n");
    fprintf(outdebug,"f 6 2 3 7\n");
    fprintf(outdebug,"f 1 2 6 5\n");
    fprintf(outdebug,"f 3 4 8 7\n");
    
    fclose(outdebug);
  }
//   exit(500);
  
//   lxlim=0;rxlim=10;
//   lylim=0;rylim=4;
//   lzlim=0;rzlim=1;
  for(i=lxlim;i<rxlim;i++){
    for(j=lylim;j<rylim;j++){
      for(k=lzlim;k<rzlim;k++){
        p.x = i*a1_rot.x +  j*a2_rot.x + k*a3_rot.x;
        p.y = i*a1_rot.y +  j*a2_rot.y + k*a3_rot.y;
        p.z = i*a1_rot.z +  j*a2_rot.z + k*a3_rot.z;
//         if ((i==1))printf("\n\nCoords: %f %f %f\n",p.x,p.y,p.z);
//         fflush(stdout);
        testetAtoms++;
        if(((testetAtoms % 1000000)==1) && idebug==1) { 
          printf("Tested %d atoms, Loop i,j,k are: %d %d %d, Flush outfile!\n",testetAtoms,i,j,k);
          fflush(out);
        }
//         if (testetAtoms>100000) exit(5);

        // (Probably wrong/incosistent comment!!) shift vector for multi-base structures
        // The shift below is the shift in the lattice origin
        // Change this shift for obtaining the right surface topology
        p.x +=  boxCenter.x;
        p.y +=  boxCenter.y;
        p.z +=  boxCenter.z;
// &&(j==1)&&(k==1)
//         if ((i==1))printf("New point Coords for i,j,k: %d, %d, %d,\t %f %f %f\n",i,j,k,p.x,p.y,p.z);
//         fflush(stdout);

        // Case of fcc,bcc,hex
        if (hasbasis==0){
          /* ...atom is in box */
          if( p.x+e >= bminimal.x && p.x+e <= bmaximal.x &&  /* rounding errors... */
              p.y+e >= bminimal.y && p.y+e <= bmaximal.y &&
              p.z+e >= bminimal.z && p.z+e <= bmaximal.z ){
            
            ihDebugFlag=0;
            p.type = inhedron(p.x,p.y,p.z,numVertices,trianglesNumber,ihDebugFlag);
//           if(p.type == 'o'){  // case of point STRICTLY OUTSIDE the domain
            if(p.type != 'o'){  // case of point NOT OUTSIDE the domain
//             if(p.type == 'i'){  // case of point STRICTLY INSIDE the domain
//             if(p.type != 'i'){  // case of point NOT INSIDE the domain
            fprintf(out,"%d %d %.16f %.16f %.16f %.16f 0.0 0.0 0.0 \n",nr++, type, m, p.x, p.y, p.z);
            }
          }
        }
        // Case of L12
        if (hasbasis==1){
          tp.x = p.x + b1_rot.x;
          tp.y = p.y + b1_rot.y;
          tp.z = p.z + b1_rot.z;
          /* ...atom is in box */
          if( tp.x+e >= bminimal.x && tp.x+e <= bmaximal.x &&
              tp.y+e >= bminimal.y && tp.y+e <= bmaximal.y &&
              tp.z+e >= bminimal.z && tp.z+e <= bmaximal.z ){
            
            ihDebugFlag=1;
            tp.type = inhedron(tp.x,tp.y,tp.z,numVertices,trianglesNumber,ihDebugFlag);
//             if ((i==1))printf("L12 point Coords for i,j,k: %d, %d, %d,\t %f %f %f %c\n",i,j,k,tp.x,tp.y,tp.z,tp.type);
//             fflush(stdout);
            if(tp.type != 'o'){  // case of point NOT OUTSIDE the domain
//             if(tp.type == 'o'){  // case of point STRICTLY INSIDE the domain
//             if(tp.type != 'i'){  // case of point NOT INSIDE the domain
              atom_type=basistype[1];
              if(atom_type==0) atom_mass=m; else atom_mass=m1;
              fprintf(out,"%d %d %.16f %.16f %.16f %.16f 0.0 0.0 0.0 \n", 
                      nr++, atom_type, atom_mass, tp.x, tp.y, tp.z);
            }
          }
          tp.x = p.x + b2_rot.x;
          tp.y = p.y + b2_rot.y;
          tp.z = p.z + b2_rot.z;
          /* ...atom is in box */
          if( tp.x+e >= bminimal.x && tp.x+e <= bmaximal.x &&
              tp.y+e >= bminimal.y && tp.y+e <= bmaximal.y &&
              tp.z+e >= bminimal.z && tp.z+e <= bmaximal.z ){
            
            ihDebugFlag=2;
            tp.type = inhedron(tp.x,tp.y,tp.z,numVertices,trianglesNumber,ihDebugFlag);
//             if ((i==1))printf("L12 point Coords for i,j,k: %d, %d, %d,\t %f %f %f %c\n",i,j,k,tp.x,tp.y,tp.z,tp.type);
//             fflush(stdout);
            if(tp.type != 'o'){  // case of point NOT OUTSIDE the domain
//             if(tp.type == 'o'){  // case of point STRICTLY INSIDE the domain
//             if(tp.type != 'i'){  // case of point NOT INSIDE the domain
              atom_type=basistype[2];
              if(atom_type==0) atom_mass=m; else atom_mass=m1;
              fprintf(out,"%d %d %.16f %.16f %.16f %.16f 0.0 0.0 0.0 \n", 
                      nr++, atom_type, atom_mass, tp.x, tp.y, tp.z);
            }
          }
          tp.x = p.x + b3_rot.x;
          tp.y = p.y + b3_rot.y;
          tp.z = p.z + b3_rot.z;
          /* ...atom is in box */
          if( tp.x+e >= bminimal.x && tp.x+e <= bmaximal.x &&
              tp.y+e >= bminimal.y && tp.y+e <= bmaximal.y &&
              tp.z+e >= bminimal.z && tp.z+e <= bmaximal.z ){
            
            ihDebugFlag=3;
            tp.type = inhedron(tp.x,tp.y,tp.z,numVertices,trianglesNumber,ihDebugFlag);
//             if ((i==1))printf("L12 point Coords for i,j,k: %d, %d, %d,\t %f %f %f %c\n",i,j,k,tp.x,tp.y,tp.z,tp.type);
//             fflush(stdout);
            if(tp.type != 'o'){  // case of point NOT OUTSIDE the domain
//             if(tp.type == 'o'){  // case of point STRICTLY INSIDE the domain
//             if(tp.type != 'i'){  // case of point NOT INSIDE the domain
              atom_type=basistype[3];
              if(atom_type==0) atom_mass=m; else atom_mass=m1;
              fprintf(out,"%d %d %.16f %.16f %.16f %.16f 0.0 0.0 0.0 \n", 
                      nr++, atom_type, atom_mass, tp.x, tp.y, tp.z);
            }
          }
          tp.x = p.x + b4_rot.x;
          tp.y = p.y + b4_rot.y;
          tp.z = p.z + b4_rot.z;
          /* ...atom is in box */
          if( tp.x+e >= bminimal.x && tp.x+e <= bmaximal.x &&
              tp.y+e >= bminimal.y && tp.y+e <= bmaximal.y &&
              tp.z+e >= bminimal.z && tp.z+e <= bmaximal.z ){
            
            ihDebugFlag=4;
            tp.type = inhedron(tp.x,tp.y,tp.z,numVertices,trianglesNumber,ihDebugFlag);
//             if ((i==1))printf("L12 point Coords for i,j,k: %d, %d, %d,\t %f %f %f %c\n",i,j,k,tp.x,tp.y,tp.z,tp.type);
//             fflush(stdout);
            if(tp.type != 'o'){  // case of point NOT OUTSIDE the domain
//             if(tp.type == 'o'){  // case of point STRICTLY INSIDE the domain
//             if(tp.type != 'i'){  // case of point NOT INSIDE the domain
              atom_type=basistype[4];
              if(atom_type==0) atom_mass=m; else atom_mass=m1;
              fprintf(out,"%d %d %.16f %.16f %.16f %.16f 0.0 0.0 0.0 \n", 
                      nr++, atom_type, atom_mass, tp.x, tp.y, tp.z);
            }
          }
        }
        
        
        
//         /* ...atom is in box */
//         if( p.x+e >= bminimal.x && p.x+e <= bmaximal.x &&  /* rounding errors... */
//             p.y+e >= bminimal.y && p.y+e <= bmaximal.y &&
//             p.z+e >= bminimal.z && p.z+e <= bmaximal.z ){
//           
//           // Test if point is inside or outside or on the domain
//           p.type = inhedron(p.x,p.y,p.z,numVertices,trianglesNumber);
//           
//           if(p.type != 'i'){  // case of point NOT INSIDE the domain
//             fprintf(out,"%d %d %.16f %.16f %.16f %.16f 0.0 0.0 0.0 \n",nr++, type, m, p.x, p.y, p.z);
//             
//             if (hasbasis==1){
//               tp.x = p.x + b1_rot.x;
//               tp.y = p.y + b1_rot.y;
//               tp.z = p.z + b1_rot.z;
//               atom_type=basistype[1];
//               if(atom_type==0) atom_mass=m; else atom_mass=m1;
//               fprintf(out,"%d %d %.16f %.16f %.16f %.16f 0.0 0.0 0.0 \n", 
//                       nr++, atom_type, atom_mass, tp.x, tp.y, tp.z);
//               atom_type=basistype[2];
//               tp.x = p.x + b2_rot.x;
//               tp.y = p.y + b2_rot.y;
//               tp.z = p.z + b2_rot.z;
//               if(atom_type==0) atom_mass=m; else atom_mass=m1;
//               fprintf(out,"%d %d %.16f %.16f %.16f %.16f 0.0 0.0 0.0 \n", 
//                       nr++, atom_type, atom_mass, tp.x, tp.y, tp.z);
//               atom_type=basistype[3];
//               tp.x = p.x + b3_rot.x;
//               tp.y = p.y + b3_rot.y;
//               tp.z = p.z + b3_rot.z;
//               if(atom_type==0) atom_mass=m; else atom_mass=m1;
//               fprintf(out,"%d %d %.16f %.16f %.16f %.16f 0.0 0.0 0.0 \n", 
//                       nr++, atom_type, atom_mass, tp.x, tp.y, tp.z);
//               atom_type=basistype[4];
//               tp.x = p.x + b4_rot.x;
//               tp.y = p.y + b4_rot.y;
//               tp.z = p.z + b4_rot.z;
//               if(atom_type==0) atom_mass=m; else atom_mass=m1;
//               fprintf(out,"%d %d %.16f %.16f %.16f %.16f 0.0 0.0 0.0 \n", 
//                       nr++, atom_type, atom_mass, tp.x, tp.y, tp.z);
//             }
//             
//             
//                 
// //               if(p.type != 'o'){  // case of point NOT OUTSIDE the domain
//               }
//         }
        
        
      }
    }
  }
  printf("Tested atoms: %d\n",testetAtoms);     
  printf("Next atom nr.: %d\n",nr);
  fclose(out);
  endtime = clock();
  cpu_time_used = ((double) (endtime - starttime)) / CLOCKS_PER_SEC;
  printf("CPU time used = %f seconds \n\n", cpu_time_used);
  return(0);

}



/*smallest including box */
void minbox(void){
   int zaehler ;
   bminimal.x =  999999999999;
   bminimal.y =  999999999999;
   bminimal.z =  999999999999;
   bmaximal.x = -999999999999;
   bmaximal.y = -999999999999;
   bmaximal.z = -999999999999;

   for( zaehler = 0 ; zaehler < numVertices ; zaehler++){
    if(verticesFile[zaehler].x < bminimal.x){
      bminimal.x = verticesFile[zaehler].x;
    }
    if(verticesFile[zaehler].y < bminimal.y){
      bminimal.y = verticesFile[zaehler].y;
    }
    if(verticesFile[zaehler].z < bminimal.z){
      bminimal.z = verticesFile[zaehler].z;
    }
    if(verticesFile[zaehler].x > bmaximal.x){
      bmaximal.x = verticesFile[zaehler].x;
    }
    if(verticesFile[zaehler].y > bmaximal.y){
      bmaximal.y = verticesFile[zaehler].y;
    }
    if(verticesFile[zaehler].z > bmaximal.z){
      bmaximal.z = verticesFile[zaehler].z;
    }
  }
}



/*read Data from file.obj */
void readData(char *inputfilename)
{
  FILE *infile;
  char buffer[255];
  char *token;
  char *res;
  int curline = 0;

  char *str;
  int i;
  int numread = 0;
  int numread2 =0;

  int testCount = 0;
  
  /* open file */
  infile = fopen(inputfilename,"r");
  if (NULL==infile){
    printf("Can't open inputfile %s\n",inputfilename);
    exit(2);
  }
  /* get parameter */
  do {
    res=fgets(buffer,255,infile);
    if (NULL == res) { break; }; /* probably EOF reached */
    curline++;
    token = strtok(buffer," \t\n");
    if (NULL == token) continue; /* skip blank lines */
    if (token[0]=='#') continue; /* skip comments */
    if (token[0]=='g') continue; /*noch offen warum da überhaupt ein g steht*/
    if (token[0]=='v')  {
      double buffer[DIM] = {0};
      for (i=0; i< DIM; i++){
        str = strtok(NULL," \t\n");
        if (str == NULL) {
          printf("Fehler beim Ecken lesen in Zeile %i\t. double vector of dim 3 expected\n",curline );
        }
      else buffer[i] = atof(str);
      }
      verticesFile[numread].x = buffer[0];
      verticesFile[numread].y = buffer[1];
      verticesFile[numread].z = buffer[2];
      numread++;
    }
       if(token[0]=='f') {
      int buffer2[POLNRMX] = {0};
      for (i=0; i< POLNRMX; i++) {
      str = strtok(NULL," \t\n");
      if (str == NULL) {
	;	//    printf("Fehler bei den Flächen.int vector of dim 3 expected\n");
      }
      else {
	buffer2[i]=atoi(str);
            }
     polygon[numread2][i] = buffer2[i];
      }
      numread2++;
      }
       testCount++;
  } while (!feof(infile));
   
  numFaces = numread2;
  numPolygon = numread2;
  numVertices = numread;
//   printf("In readData: numFaces=%d, numPolygon=%d, numVertices=%d \n",numFaces,numPolygon,numVertices);
  fclose(infile);
}

/*convert data in inhedron Format */
void 	ReadVertices( void )
{
 int i;
  for(i = 0; i <numVertices ; i++){
    Vertices[i][0] = verticesFile[i].x; 
    Vertices[i][1] = verticesFile[i].y;
    Vertices[i][2] = verticesFile[i].z;
  }
}
/*convert data in inhedron Format*/
void ReadFaces( void )
{
  int	i,j,k;
  double   w; /* temp storage for coordinate. */
  
  for(i = 0; i < trianglesNumber; i++){
    Faces[i][0] = triangles[i].x;
    Faces[i][1] = triangles[i].y;
    Faces[i][2] = triangles[i].z;
   
    /* Compute bounding box. */
    /* Initialize to first vertex. */
    for ( j=0; j < 3; j++ ) {
       Box[i][0][j] = Vertices[ Faces[i][0]-1 ][j];
       Box[i][1][j] = Vertices[ Faces[i][0]-1 ][j];
    }
    /* Check k=1,2 vertices of face. */
    for ( k=1; k < 3; k++ )
    for ( j=0; j < 3; j++ ) {
       w = Vertices[ Faces[i][k]-1 ][j];
       if ( w < Box[i][0][j] ) Box[i][0][j] = w;
       if ( w > Box[i][1][j] ) Box[i][1][j] = w;
    }
  }
}

/*getting the highest number of corners of the polygons */
int PolyederVertices(void)
{
  int i,j,cornerCount;
  
  maxpolyedervertices = 0;
  for(i = 0 ; i < numPolygon;i++){
    cornerCount = 0;
    for(j=0;j<POLNRMX;j++){
	    if(polygon[i][j] != 0 ){
	      cornerCount++;
	    }
	  }
    if(cornerCount > maxpolyedervertices){
	    maxpolyedervertices = cornerCount;
	  }
  }
//   printf("maxeckenzahl %i\t \n", maxpolyedervertices);
  return maxpolyedervertices;

}



    
