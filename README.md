# *nanoSCULPT* #

Welcome to the official repository of *nanoSCULPT*, a tool/methodology to generate complex and realistic structures for atomistic simulations. This README file provides a quick summary of *nanoSCULPT*, followed by brief instructions to help you set up and run *nanoSCULPT* on your computer. For more detailed information, please see the [wiki](https://bitbucket.org/arunpksh/nanosculpt/wiki/) of this repository.

## Brief summary ##

*nanoSCULPT* was originally hosted at the individual development groups of the tool, viz. FAU Erlangen-Nürnberg and Uni Stuttgart. This repository now brings the efforts of the two groups into one place.

####Contributors####
Arun Prakash, Materials Science and Engineering, Institute I, Friedrich-Alexander-Universität Erlangen-Nürnberg

Martin Hummel, IMWF, Universität Stuttgart

Siegfried Schmauder, IMWF, Universität Stuttgart

Erik Bitzek, Materials Science and Engineering, Institute I, Friedrich-Alexander-Universität Erlangen-Nürnberg


`Current stable version: 1.1`

## How do I get set up? ##

To obtain nanoSCULPT, you need an invite. Since you are able to see this README, you should already have been invited. Now that you have access to the repository, use `git` to clone the repository on your local machine. The code can then be compiled as follows:

```bash
make clean
make
```
For more details, please visit the [wiki](https://bitbucket.org/arunpksh/nanosculpt/wiki/).

####System requirements####

* Gnu C compiler (gcc)
* gmake/make


####Using and citing nanoSCULPT####

If you use *nanoSCULPT* for your research, please cite *nanoSCULPT* in your publications as follows:
> A. Prakash, M. Hummel, S. Schmauder, E. Bitzek [2016],

> *NanoSCULPT*: A methodology for generating complex realistic configurations for atomistic simulations,

> **MethodsX, 3**, 219-230
