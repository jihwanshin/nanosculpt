//***************************************************************************************************************************
// nanoSCULPT: A tool to generate complex and realistic configurations for atomistic simulations
// Copyright (C) 2011-2015
// @authors: A. Prakash, M. Hummel, S. Schmauder and E. Bitzek, 
//
//
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 * 
 */
/*-----------generate_rotMat_randomOrient.c---------------*/
/* Program to create random rotation matrices  *
 * for the crystallographic orientations of a polycrystalline sample *
 * Erik Bitzek, Sept 2011 */
// Arun Prakash Feb 2012
// Corrected the range of psi - only from 0-180°
// NOTE: To rotate from global to crystal frame use Rmat.T
//       To rotate from crystal to global frame user Rmat
/* compile with 
   gcc -o generate_rotMat_randomOrient generate_rotMat_randomOrient.c -lm
*/
//***************************************************************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NMAX  1000 /* maximum number of grains */


/* creating a rotation matrix from Bunge Euler Angles, Definition see A. Rollett */
void eulerangles2matrix(double matrix[3][3], double P1, double P , double P2)
{
  double C1, C,C2,S1,S,S2;
  C1=cos(P1);
  C=cos(P);
  C2=cos(P2);
  S1=sin(P1);
  S=sin(P);
  S2=sin(P2);

  matrix[0][0]=C1*C2-S1*S2*C;
  matrix[0][1]=S1*C2+C1*S2*C;
  matrix[0][2]=S2*S;
  matrix[1][0]=-C1*S2-S1*C2*C;
  matrix[1][1]=-S1*S2+C1*C2*C;
  matrix[1][2]=C2*S;
  matrix[2][0]=S1*S;
  matrix[2][1]=-C1*S;
  matrix[2][2]=C;
}


int main(int argc, char **argv)
{
   FILE *outfile;  
   int n,seed,i,j,k;
   double pi,phi,psi,theta;
   double mat[3][3];

   pi = acos(-1.0);

  /*educate user */
  if (argc!=2)
    {
      printf("Usage: %s number_of_grains \n",argv[0]);
      exit(20);
    }
  else
    {
      sscanf(argv[1], "%d", &n);
    }

  /* IO */
  if((outfile=fopen("rotmat_RandomOrientation.dat","w"))==NULL)
    { 
      fprintf(stderr,"rotmat_RandomOrientation.dat\n");
      exit(1);
    }

  fprintf(outfile,"# grain rotation matrix\n");

  /* initialize random number generator */
  seed = 523942832;
  srand48(seed);

  for (i=0;i<n;i++)
    {
      phi=2.0*pi*drand48();
// Arun, 8-Feb-2013: psi, the second Euler angle goes from 0-180!!
//      psi=2.0*pi*drand48();
      psi=pi*drand48();
      theta=2.0*pi*drand48();
      eulerangles2matrix(mat,phi,psi,theta);

      fprintf(outfile,"%d\n",i);
      for (k=0;k<3;k++)
	fprintf(outfile," %.16lf %.16lf %.16lf\n",mat[k][0],mat[k][1],mat[k][2] );
    }
 
}

