#! /usr/bin/awk -f
#***************************************************************************************************************************
# nanoSCULPT: A tool to generate complex and realistic configurations for atomistic simulations
# Copyright (C) 2011-2015
# @authors: A. Prakash, M. Hummel, S. Schmauder and E. Bitzek, 
#
#
# *
# * This program is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# * 
# * This program is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# * 
# * You should have received a copy of the GNU General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>
# * 
# *
#----------------------------------
# program to create OBJ configurations out of the QHull output 
# Erik Bitzek, Sept. 2011
# Arun Prakash, Feb. 2012
# Arun: Recreating this file in Aug 2014 after I accidentally deleted it!
# Arun: Added the possibility to account for GB areas
#----------------------------------
#***************************************************************************************************************************


BEGIN {
    i=-1;
    k=-1;
    m=0;
    gn=grainnr;
}

{
  if (NR ==2) {
    nvertices=$1;
    nfacets=$2;
    i=0;
    k=0;
#     m=0;
#     print "nvertices" nvertices;
#     print "nfacets" nfacets;
  }
  else if(i>=0){
    if(i<nvertices) {
      print "v " $0;
      i++;
    }
    else if(i==nvertices){
      print "g grain" gn;
      i++;
    }
    if((i>nvertices) && (k<nfacets)){
      printf("f "); for(j=2;j<=NF;j++){printf("%d ", $j+1)};
      printf("\n");
      k++;
    }
    if((m<nfacets) && (k>nfacets)){
      printf("#a %f\n",$1);
      m++;
    }
    if((NR==(nvertices+nfacets+3)) && (k==nfacets)) {
#     if((m==0) && (k==nfacets)) {
      printf("#Total num of facets: %d -- area of each as below %d\n",k,$1);
      k++;
    }
  }
}

END{
#       print "vertices " i-1 " nvertices " nvertices;
#       print "facets " k-1 " nfacets " nfacets;
}
