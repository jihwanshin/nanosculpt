//***************************************************************************************************************************
// nanoSCULPT: A tool to generate complex and realistic configurations for atomistic simulations
// Copyright (C) 2011-2015
// @authors: A. Prakash, M. Hummel, S. Schmauder and E. Bitzek, 
//
//
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 * 
 */
/*-----------------------generate_random_grain_centers_w_pbc.c-------------------------*/
/* Program to create random grain centers for the Voronoi construction *
 * Erik Bitzek, Sept 2011 */
/* Arun Prakash, Mar 2016 */
/* compile with 
   gcc -o generate_random_grain_centers_w_pbc generate_random_grain_centers_w_pbc.c -lm */
//***************************************************************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NMAX  1000 /* maximum number of grains */

int main(int argc, char **argv)
{

  int n,i,ix,iy,iz,numgrains,ngrains;
  double lx,ly,lz,vave,dave,r;
  int seed;
  FILE *outfile;
  double x[NMAX],y[NMAX],z[NMAX];

  /* initialize random number generator */
  seed = 549755813;
  srand48(seed);

  /*educate user */
  if (argc!=5)
    {
      printf("Usage: %s Lx Ly Lz number_of_grains \n",argv[0]);
      exit(20);
    }
  else
    {
      sscanf(argv[1], "%lf", &lx);
      sscanf(argv[2], "%lf", &ly);
      sscanf(argv[3], "%lf", &lz);
      sscanf(argv[4], "%d",  &n);
      printf("      Creating %d grain centers in a %lfx%lfx%lf Angstrom^3 box\n",n,lx,ly,lz);
      vave=lx*ly*lz/n;
      dave=pow(vave*3.0/4.0/3.14159265,1.0/3.0)*2.0;
      printf("      Average grain volume %lf Angstrom^3 average (spherical) grain diameter: %lf Angstrom\n ",vave,dave);
      printf("      Grain centers are written out to grain_centers_pbc.dat\n");
    }
  
  /* IO */
  if((outfile=fopen("grain_centers_pbc.dat","w"))==NULL)
    { 
      fprintf(stderr,"Can't write grain_centers_pbc.dat\n");
      exit(1);
    }
  ngrains =0;
  fprintf(outfile,"3\n %d\n",n*27);
  for(i=0;i<n;i++)
    {
      r=drand48();
      x[i]= r*lx - 0.5*lx;
      r=drand48();
      y[i]= r*ly - 0.5*ly;
      r=drand48();
      z[i]= r*lz - 0.5*lz;
      
      fprintf(outfile,"%lf %lf %lf\n",x[i],y[i],z[i]);
      ngrains++;
    }

  for(ix=-1;ix<=1;ix++)
    for(iy=-1;iy<=1;iy++)
      for(iz=-1;iz<=1;iz++)
	if(! (ix==0 && iy==0 && iz==0))
	  {
	    for (i=0;i<n;i++)
	      {
	       fprintf(outfile, "%lf %lf %lf\n",
		       ix*lx + x[i],
		       iy*ly + y[i],
		       iz*lz + z[i]);
	       ngrains++;
	      }

	  }
  printf("      wrote %d grains under pbc in all direction, should be %d*27=%d\n",ngrains,n,n*27);
}
